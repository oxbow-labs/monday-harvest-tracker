const account = '316599'; // Harvest Account ID
const csv_input = document.getElementById('monday-harvest-csv');
let error = false;
let errors = [];

csv_input.onchange = function () {

  let file = this.files[0];
  let reader = new FileReader();

  reader.onload = function (progressEvent) {

    error = false;
    errors = [];

    let lines = this.result.split(/\r\n|\n/);
    let header = lines[0].split(',');
    let headers = {};

    for (let i = 0; i < header.length; i++) {
      headers[header[i]] = i;
    }

    for (let i = 1; i < lines.length; i++) {
      let line = lines[i].split(',');
      track(
          line[headers['user_id']],
          line[headers['project_id']],
          line[headers['pulse_id']],
          line[headers['permalink']],
          line[headers['start_date']],
          line[headers['start_time']],
          line[headers['end_time']],
          line[headers['notes']]
      );
    }

    csv_input.value = '';

    if (error) {
      alert('Error processing entries for pulse ' + errors.join(', '));
    }
    else {
      alert('Added ' + (lines.length - 1) + ' time entries');
    }
  };

  reader.readAsText(file);
};

let trackError = function (pulse_id) {
  error = true;
  errors.push(pulse_id);
}