const account = '316599'; // Harvest Account ID
const board_id = 'current-board-component-id';
let board = document.getElementById(board_id);
let project_id = false; // Harvest Project ID

let pulse_observer = new window.MutationObserver(function (mutations, observer) {
  mutations.forEach(mutation => {
    if (mutation.addedNodes.length) {
      [...board.getElementsByClassName('pulse-id-cell-component')].forEach(cell => {
        if (!cell.getElementsByClassName('oxbow-tracker').length) {
          let pulse_id = cell.innerHTML;
          let button = document.createElement('a');
          button.classList.add('oxbow-tracker');
          button.innerHTML = 'Track';
          button.addEventListener('click', function () {
            let title = document.querySelector('#pulse-' + pulse_id + ' .name-cell-text');
            trackStart(project_id, pulse_id, title.innerText);
          });
          cell.appendChild(button);
        }
      });
    }
  });
});

let board_observer = new window.MutationObserver(function (mutations, observer) {
  mutations.forEach(mutation => {
    if (mutation.target.id == board_id) {
      board = document.getElementById(board_id);
      let header = document.getElementById('board-header');
      if (header && header.innerHTML) {
        let match = header.innerHTML.match(/\[harvest_project:\d*\]/);
        if (!match) {
          return;
        }
        match = match[0];
        let tmp_project_id = match.length ? match.split(':')[1].slice(0, -1) : false;
        if (tmp_project_id && tmp_project_id != project_id) {
          project_id = tmp_project_id;
          //console.log('Project ID: ' + project_id);
          pulse_observer.disconnect();
          pulse_observer.observe(board, {
            subtree: true,
            childList: true
          });
        }
      }
      else {
        project_id = false;
      }
    }
  });
});


board_observer.observe(document, {
  subtree: true,
  childList: true
});

let trackStart = function (track_project_id, track_pulse_id, track_note) {

  let d = new Date();

  let date = [
    d.getFullYear(),
    ('0' + (d.getMonth() + 1)).slice(-2),
    ('0' + d.getDate()).slice(-2)
  ].join('-');

  let time = [
    ('0' + d.getHours()).slice(-2),
    ('0' + d.getMinutes()).slice(-2)
  ].join(':');

  let link = window.location.href;
  link = link.indexOf('/pulses') > 0 ? link.slice(0, link.indexOf('/pulses')) : link;
  link += '/pulses/' + track_pulse_id;

  track('', track_project_id, track_pulse_id, link, date, time, '', track_note);
};

let trackError = function(pulse_id) {
  alert('Error starting tracker for pulse ' + pulse_id);
}