const track = function (
    track_user_id = '',
    track_project_id,
    track_pulse_id,
    track_url,
    track_date,
    track_start,
    track_end = '',
    track_note
) {

  let data = new FormData();
  if (track_user_id.length) {
    data.append('user_id', track_user_id);
  }
  data.append('project_id', track_project_id);
  data.append('task_id', '2250293'); // dev/config
  data.append('spent_date', track_date);
  data.append('started_time', track_start);
  if (track_end.length) {
    data.append('ended_time', track_end);
  }
  data.append('external_reference[id]', track_pulse_id);
  data.append('external_reference[permalink]', track_url);
  data.append('notes', track_note);

  let xhr = new XMLHttpRequest();
  xhr.open("POST", 'https://api.harvestapp.com/v2/time_entries', true);
  xhr.setRequestHeader('Harvest-Account-Id', account);
  xhr.setRequestHeader('Authorization', 'Bearer ' + harvest_auth);

  xhr.onreadystatechange = function () {
    if (this.readyState === XMLHttpRequest.DONE && this.status === 201) {
      let button = document.querySelector('#pulse-' + track_pulse_id + ' .oxbow-tracker');
      if (button) {
        button.classList.add('running');
        setTimeout(function (b) {
          b.classList.remove('running');
        }, 3000, button);
      }
    }
    else if (this.readyState === XMLHttpRequest.DONE) {
      trackError(track_pulse_id);
    }
  };

  xhr.send(data);
};