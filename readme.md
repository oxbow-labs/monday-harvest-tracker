# Monday Harvest Time Tracker

## Extension Installation

1. Clone the repository to your local machine

1. Copy `example.tokenScript.js` to `tokenScript.js`

1. Obtain a Harvest personal access token: https://id.getharvest.com/developers

1. Set the variable `harvest_auth` in `tokenScript.js` to your Harvest personal access token

1. Open the Chrome extensions page: `chrome://extensions/`

1. Enable developer mode (top right corner of the page)

1. Add your Monday Harvest Tracker extension

## Extension Update

1. Update local repository

1. Open the Chrome extensions page: `chrome://extensions/`

1. Locate the `Monday Time Tracker`

3. Click the reload extension button (lower right corner of card) 

## Monday Configuration

1. Add the Harvest project ID to the board's description in the format `[harvest_project:{project_id}]`

1. Add the `Item ID` column to the board